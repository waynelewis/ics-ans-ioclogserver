#!/bin/bash

ANSIBLE_VERSION="2.4.2.0"
REPOSITORY="ics-ans-ioclogserver"
REPOSITORY_REMOTE_URL="https://gitlab.esss.lu.se/waynelewis/"
REPOSITORY_BRANCH="master"

usage() {
  cat <<EOF
  Usage : bootstrap-local [--no-run]

  Options:

    -h | --help    : show this help

    -n | --no-run  : don't run the ansible-playbook command
                     allow to change variables before to run the playbook

EOF
}

NO_RUN=false
while true
do
  case "$1" in
    -h | --help ) usage; exit;;
    -n | --no-run ) NO_RUN=true; shift;;
    -- ) shift; break;;
    * ) break;;
  esac
done


if [[ "$(whoami)" != "root" ]]
then
  echo "ERROR! $0 shall be run as root."
  exit 1
fi

# Install ansible-playbook and ansible-galaxy PEX files
for exe in ansible-playbook ansible-galaxy
do
  echo "Installing $exe"
  curl --fail -o /usr/local/bin/${exe} https://artifactory.esss.lu.se/artifactory/swi-pkg/ansible-releases/${ANSIBLE_VERSION}/${exe}
  chmod a+x /usr/local/bin/${exe}
done

echo "Creating ansible.cfg"
mkdir -p /etc/ansible
cat <<EOF > /etc/ansible/ansible.cfg
[defaults]
inventory      = /etc/ansible/hosts

# logging is off by default unless this path is defined
# if so defined, consider logrotate
log_path = /var/log/ansible.log
EOF

echo "Creating Ansible inventory"
cat <<EOF > /etc/ansible/hosts
[ioclogserver]
localhost ansible_connection=local
EOF

# Configure logrotate
cat << EOF > /etc/logrotate.d/ansible
/var/log/ansible.log {
    missingok
    notifempty
    size 100k
    monthly
    create 0666 root root
}
EOF

echo "Cloning ${REPOSITORY} from ${REPOSITORY_REMOTE}"
yum install -y git
rm -rf /root/${REPOSITORY}
git clone ${REPOSITORY_REMOTE_URL}${REPOSITORY}.git /root/${REPOSITORY}

cd /root/${REPOSITORY}
git checkout ${REPOSITORY_BRANCH}
cp -r group_vars playbook.yml /etc/ansible/

echo "Downloading Ansible roles"
/usr/local/bin/ansible-galaxy install -r /root/${REPOSITORY}/roles/requirements.yml --force -p /etc/ansible/roles/

if [[ "$NO_RUN" == "true" ]]
then
  echo "Ansible playbook installed. You can set variables in /etc/ansible/host_vars/localhost and run the command:"
  echo '"/usr/local/bin/ansible-playbook /etc/ansible/playbook.yml"'
else
  echo "Running Ansible playbook"
  /usr/local/bin/ansible-playbook /etc/ansible/playbook.yml
fi
