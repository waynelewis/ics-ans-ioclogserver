import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('ioclogserver')


def test_ioclog_server_running(host):
    assert host.service('ioclogserver').is_running
